﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }


    public class Calculate
    {
        public static double FirstNumber { get; set; }

        public static double SecondNumber { get; set; }

        public static double Result { get; set; }

        public static void GetFirstNumber(double a)
        {
            FirstNumber = a;
        }

        public static void GetSecondNumber(double b)
        {
            SecondNumber = b;
        }

        public static double Sum()
        {
            return Result = FirstNumber + SecondNumber;
        }

        public static double Substraction(double a, double b)
        {
             return a > b ? a - b : 0 ;
        }

        public static double Multiplication(double a, double b)
        {
            return a * b;
        }

        public static double Division(double a, double b)
        {
            return a > b ? a / b : 0;
        }

        public static double step(double a, double b)
        {
            return Math.Pow(a, b);
        }

        public static double koren(double a, double b)
        {
            return Math.Pow(a, 1 / b);
        }

    }

}
