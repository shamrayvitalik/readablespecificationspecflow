﻿using System;
using TechTalk.SpecFlow;
using Calculate;
using Xunit;

namespace Calculator.feature
{
    [Binding]
    public class SpecFlowFeatureSteps
    {
        private double result { get; set; }

        [Given(@"I have entered (.*) to the first input field Calculater")]
        public void GivenIHaveEnteredIntoTheCalculator(double number)
        {
            Calculate.Calculate.GetFirstNumber(number);
            Assert.Equal(number, Calculate.Calculate.FirstNumber);
        }

        
        [Given(@"I have entered (.*) to the second input field Calculater")]
        public void GivenIHaveAlsoEnteredIntoTheCalculator(double n)
        {
            Calculate.Calculate.GetSecondNumber(n);
            Assert.Equal(n, Calculate.Calculate.SecondNumber);
        }

        [When(@"I press Sum")]
        public void WhenIPressAdd()
        {
            result = Calculate.Calculate.Sum();
        }

        [Then(@"The result should be (.*) in the third field Calculater")]
        public void ThenTheResultShouldBeOnTheScreen(double number)
        {
            Assert.Equal(result, number);
        }
    }
}
